IT solutions focussing on internet content filtering, employee monitoring software, network security and employee productivity. Improve employee and student productivity, understand your employee's web activities, and prevent malicious attacks.

Address: CurrentWare Inc, PO Box 30024, King St PO, Toronto, ON M5V 0A3, Canada

Phone: 613-368-4300

Website: http://currentware.com